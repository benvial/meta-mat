
.PHONY: all autobuild clean tex pdf

SHELL := /bin/bash
TEXCMD = pydflatex -x
CHECKCMD = $(TEXCMD) -l
BIBTEXCMD = bibtex
ifndef VERBOSE
.SILENT:
endif
ifndef TEXOUTPUT
NOOUT = > /dev/null 2>&1
endif


SRC = $(PWD)/src
NAME = slides
BIBDATABASE = biblio.bib
BIB = biblio.bib
MACROS = macros
BASENAME=$(notdir $(PWD))
PDF_NAME = $(BASENAME).pdf
HTML_NAME = $(BASENAME).html
HTML_SLIDES_NAME = $(BASENAME)_slides.html
HTML_SLIDES_CONVERTED_NAME = $(BASENAME)_pdf_slides.pdf
PANDOC_SRC = $(SRC)/pandoc

REVEAL_SRC = "https://unpkg.com/reveal.js"
ifndef REMOTE
REVEAL_PATH= $(HOME)/prg/reveal.js
REVEAL_SRC = $(subst $(HOME),http://localhost:8000,$(REVEAL_PATH))
endif

OUTPUT = $(PWD)/build
BIB_PATH = $(HOME)/.bib
TEX_SHARE = $(SRC)/tex
FILES = $(OUTPUT)/$(NAME).md
THEME := $(shell pandoc --template=$(PANDOC_SRC)/metadata.pandoc-tpl $(SRC)/include/00_header.md --quiet | jq -r '.theme')
ifndef THEME
THEME = null
endif
ifeq ($(THEME),null)
THEME = base
endif

FLAGS = -t beamer --pdf-engine=xelatex \
		--template=$(OUTPUT)/themes/base.tex --biblatex \
		--standalone --citeproc \
		--slide-level=2 \
		--highlight-style $(TEX_SHARE)/themes/$(THEME).theme



watch: autobuild read
	bash autobuild pdf watch 
	

watch-reveal: autobuild-reveal read-reveal
	bash autobuild reveal watch-reveal
	


autobuild-reveal:
	bash autobuild reveal


autobuild:
	bash autobuild pdf
	
quick: autobuild-fast read
	bash autobuild fast watch 

autobuild-fast:
	bash autobuild fast

all: pdf


bib:
	rm -f $(OUTPUT)/$(BIB)
	# cp -f $(BIB_PATH)/$(BIBDATABASE) $(OUTPUT)/$(BIB) || echo Biblio not found, using $(SRC)/$(BIB)
	# cp -f $(BIB_PATH)/$(BIBDATABASE) $(SRC)/$(BIB)
	cp -f $(SRC)/$(BIB) $(OUTPUT)/$(BIB)


init:
	mkdir -p $(OUTPUT)
	touch $(OUTPUT)/$(NAME).md
	cp -rf $(SRC)/* $(OUTPUT)
	cat /dev/null > $(OUTPUT)/$(NAME).md
	sed -e '$$s/$$/\n/' -s $(OUTPUT)/include/*.md > $(OUTPUT)/$(NAME).md
	cp -f $(TEX_SHARE)/$(MACROS).tex $(OUTPUT)
	cp -rf $(TEX_SHARE)/themes $(OUTPUT)/
	

tex: init bib
	pandoc -o $(OUTPUT)/$(NAME).tex $(FLAGS) $(FILES) $(NOOUT)


pdf: tex
	cd $(OUTPUT) && \
	$(TEXCMD) $(NAME) $(NOOUT) && \
	$(BIBTEXCMD) $(NAME) $(NOOUT) && \
	$(TEXCMD) $(NAME) $(NOOUT) && \
	$(TEXCMD) $(NAME) $(NOOUT)  && \
	cd .. && mv -f $(OUTPUT)/$(NAME).pdf $(PDF_NAME)
	
fast: tex
	cd $(OUTPUT) && \
	$(TEXCMD) $(NAME) $(NOOUT) && \
	cd .. && mv -f $(OUTPUT)/$(NAME).pdf $(PDF_NAME)

read:
	xdg-open $(PDF_NAME) $(NOOUT)
	

read-html:
	xdg-open $(OUTPUT)/$(HTML_NAME) $(NOOUT)
	

clean:
	rm -rf $(OUTPUT) && rm -f $(PDF_NAME)


check:
	$(CHECKCMD) $(OUTPUT)/$(NAME).tex || true

macros: 
	mkdir -p $(OUTPUT)
	rm -f $(OUTPUT)/macros.html
	cd $(TEX_SHARE)/ && ./make_html
	cp -f $(TEX_SHARE)/$(MACROS).html $(OUTPUT)/

html: bib
	pandoc --filter=pandoc-citeproc --filter=pandoc-xnos \
	--filter=pandoc-latex-environment --csl=$(TEX_SHARE)/biblio.csl \
	--include-before-body=$(TEX_SHARE)/macros.html --biblatex \
	--bibliography=$(OUTPUT)/biblio.bib --mathjax \
	--include-in-header=$(PANDOC_SRC)/mathjax_config.html \
	--template=$(PANDOC_SRC)/template.html5 \
	-o $(OUTPUT)/$(HTML_NAME) $(FILES) #$(NOOUT)


ifdef REMOTE
revealtheme:
	echo '<link rel="stylesheet" href="$(subst $(HOME),,pandoc/css/reveal/$(THEME).css)" id="theme">' > $(OUTPUT)/theme.css
else
revealtheme:
	echo '<link rel="stylesheet" href="$(subst $(HOME),http://localhost:8000,$(PANDOC_SRC)/css/reveal/$(THEME).css)" id="theme">' > $(OUTPUT)/theme.css
endif

reveal: init macros bib revealtheme
	# cp $(PANDOC_SRC)/css/reveal/$(THEME).css $(HOME)/doc/pandoc/slides/reveal.js/
	pandoc -t revealjs -s -o $(OUTPUT)/$(HTML_SLIDES_NAME) $(FILES) \
	--template=$(PANDOC_SRC)/slides.revealjs \
	-V revealjs-url=$(REVEAL_SRC) \
	-V margin=0.001 \
	--mathjax --citeproc \
	--reference-location=block \
	--slide-level=3 \
	--csl=$(TEX_SHARE)/note.csl \
	--highlight-style $(TEX_SHARE)/themes/$(THEME).theme \
	--include-before-body=$(TEX_SHARE)/macros.html --biblatex \
	--include-in-header=$(OUTPUT)/theme.css \
	--bibliography=$(OUTPUT)/biblio.bib
	python $(SRC)/filters/postpro_reveal.py $(OUTPUT)/$(HTML_SLIDES_NAME)
	# --include-in-header=$(subst $(HOME),http://localhost:8000,$(PANDOC_SRC)/mathjax_config.html) \
	
	# sleep 1 && curl $(subst $(HOME),http://localhost:8000,$(OUTPUT)/$(HTML_SLIDES_NAME)) -s > /dev/null 
	
	# -M "panflute-filters=reveal_slide_bg.py" \
	# -M "panflute-path=$(SRC)/filters" \
	# --filter panflute  \
	
read-reveal:
	xdg-open $(subst $(HOME),http://localhost:8000,$(OUTPUT)/$(HTML_SLIDES_NAME)) 
	
# read-reveal: link
# 	cd $(OUTPUT) &&  \
# 	kill-port 8001 && npm start -- --port=8001 & sleep 1  &&  \
# 	xdg-open http://localhost:8001/$(HTML_SLIDES_NAME) 


reload-reveal:
	echo reload
	xdotool windowactivate `xdotool search --name "Mozilla Firefox" | head -1` && sleep 0.1 && \
	xdotool key F5

reveal-pdf:
	decktape $(subst $(HOME),http://localhost:8000,$(OUTPUT)/$(HTML_SLIDES_NAME)) -s 1920x1080 $(OUTPUT)/$(HTML_SLIDES_CONVERTED_NAME)



# echo `xdotool search --name "Mozilla Firefox"`
# for id in `xdotool search --name "Mozilla Firefox"` ; do \
# 	echo $$id  && xdotool windowactivate $$id && sleep 1 && xdotool key F5; \
# done


link:
	ln -sf $(REVEAL_PATH)/node_modules build/
	ln -sf $(REVEAL_PATH)/gulpfile.js build/
	ln -sf $(REVEAL_PATH)/package.json build/



## Push to gitlab
save:
	@git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG"
	@git push origin main