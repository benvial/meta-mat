#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
import os
import numpy as np
import nannos as nn
import matplotlib.pyplot as plt

plt.ion()

plt.close("all")

os.system(f"rm -f *.png")
fig, ax = plt.subplots(1, 2, figsize=(4.5, 2))
N = 301
p = np.random.rand(N, N)
p = (p - p.min()) / (p.max() - p.min())
aniso = 5
for i, r in enumerate(np.linspace(0, N / 7, 30)):
    try:
        cb1.remove()
        cb2.remove()
    except:
        pass
    pf = nn.optimize.apply_filter(p, r)
    pf = (pf - pf.min()) / (pf.max() - pf.min())
    pfa = nn.optimize.apply_filter(p, (r,r/aniso))
    pfa = (pfa - pfa.min()) / (pfa.max() - pfa.min())
    ax[0].clear()
    plt.sca(ax[0])
    plt.imshow(pf, vmin=0, vmax=1, cmap="Reds")
    plt.axis("off")
    plt.title(f"$R/a = {{{r:0.3f}}}$")
    cb1 = plt.colorbar()
    ax[1].clear()
    plt.sca(ax[1])
    plt.imshow(pfa, vmin=0, vmax=1, cmap="Reds")
    plt.axis("off")
    plt.title(f"$R/a = ({{{r:0.3f}}}, {{{r/aniso:0.3f}}})$")
    cb2 = plt.colorbar()

    if i==0:
        plt.tight_layout()
    name = "filter" + str(i).zfill(4) + ".png"
    plt.savefig(name)
    plt.pause(0.01)


# # os.makedirs(f"figs", exist_ok=True)
os.system(f"convert -delay 40 -loop 0 *.png filter.gif")
os.system(f"convert filter.gif ../fig/filter.mp4")
# os.system(f"rm -f animation_nannos.gif")


os.system(f"rm -f *.png")

x = np.linspace(0,1,1010)

fig, ax = plt.subplots(1, 2, figsize=(4.5, 2))


olds = []

for N in range(8):
    try:
        cb.remove()
    except:
        pass
    pp = nn.optimize.project(pf, 2**N)
    ax[0].clear()
    plt.sca(ax[0])
    pf1d = nn.optimize.project(x, 2**N)
    olds.append(pf1d)
    for pp1 in olds:
        plt.plot(x,pp1,alpha=0.12,c="#c13149")
    plt.plot(x,pf1d)
    plt.ylabel(r"projected $\hat{p}$")
    plt.xlabel(r"$\tilde{p}$")
    plt.sca(ax[1])
    ax[1].clear()
    plt.imshow(pp, vmin=0, vmax=1, cmap="Reds")
    plt.axis("off")
    plt.suptitle(rf"$\beta = 2^{{{N}}}$")
    cb = plt.colorbar()
    if N==0:
        plt.tight_layout()


    name = "proj" + str(N).zfill(4) + ".png"
    plt.savefig(name)
    plt.pause(0.01)





os.system(f"convert -delay 200 -loop 0 *.png proj.gif")
os.system(f"convert proj.gif ../fig/proj.mp4")

os.system(f"rm -f *.png")

os.system(f"mv *.gif ../fig/")
