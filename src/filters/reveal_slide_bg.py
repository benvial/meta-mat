#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
from panflute import *

import tinycss2
import sys
# print(sys.argv)

def get_colors(css_path):
    with open(css_path) as sheet:
        css = tinycss2.parse_stylesheet_bytes(sheet.read().encode("utf-8"))


    for c in css[0]:
        try:
            a = c.prelude[0].value
            b = c.prelude[1].lower_value
            isroot = a + b == ":root"
            if isroot:
                break
        except:
            pass

    colors = {}
    colorname=None
    tokens=["--brand-primary", "--brand-secondary"]
    for v in c.content:
        if v.type == "ident" or v.type == "hash" or v.type == "literal":
            s = v.serialize()
            for k in tokens:
                if s == k:
                    colorname = s
                if v.type == "hash" and colorname:
                    colors[colorname]= s
                    colorname = None
    return colors


def color_bg(elem, doc,colors):
    if type(elem) == Header:
        # print(elem.classes)
        if "part" in elem.classes:
            elem.attributes.update({'data-background-color': colors["--brand-secondary"]})
        
        if "sec" in elem.classes:
            elem.attributes.update({'data-background-color': colors["--brand-primary"]})


def main(doc=None):
    theme = doc.get_metadata('theme', "animate")
    import os
    os.system("ls")
    css_path=f"../pandoc/css/reveal/{theme}.css"
    css_path=f"/home/bench/doc/pandoc/slides/template/src/pandoc/css/reveal/{theme}.css"
    colors = get_colors(css_path)
    return run_filter(color_bg, doc=doc,colors=colors)

if __name__ == "__main__":
    main()
