---
title: Topology optimization of electromagnetic metamaterials
author:
  - name: Benjamin Vial
    affiliation: Department of Mathematics, Imperial College London
    school:
    group:
    location:
    email: b.vial@imperial.ac.uk
date:
theme: imperial
imperial: true
width: 1080
height: 800
title-slide-attributes:
  data-background: "#003E74"
---
