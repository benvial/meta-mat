
## Introduction

What is topology optimization?


> <div style="font-size: 100%;">A mathematical method that optimizes material layout within a given design space, for a given set of sources, boundary conditions and constraints with the goal of maximizing the performance of the system</div>


::: notes

Different from shape optimization and sizing optimization in the sense that the design can attain any shape within the design space, instead of dealing with predefined configurations and parametrization of the geometry

:::


## {data-background-image="https://comet-fenics.readthedocs.io/en/latest/_images/cantilever_beam.gif"} 

<div style="min-height: 53vh;"></div>
<div style="font-size: 70%">Topology optimization `Hello World!`: Maximizing a beam stiffness with fixed volume fraction</div>
<div style="margin: -3vh;">[@bleyer2018numericaltours]</div>


### {data-background-color=black data-background-image="https://www.wernersobek.com/wp-content/uploads/resized/2021/05/1306_29be6-1239x826-c-default.jpg" data-background-opacity=1}

<div style="text-transform: uppercase">Structural Engineering</div>
<div style="font-size: 70%">Qatar National Convention Centre</div>


### {data-background-color=black data-background-image="fig/wing.webp" data-background-opacity=0.5}

<div style="min-height: 20vh;"></div>
<div style="text-transform: uppercase">Aeronautics</div>
<div style="font-size: 70%">Airplane wing</div>
<div style="min-height: 20vh;">[@aageGigavoxelComputationalMorphogenesis2017]</div>


### 

<div style="min-height: 20vh;"></div>
<div style="text-transform: uppercase">Photonics</div>
<!-- <div style="font-size: 70%">Airplane wing</div> -->
<div style="min-height: 20vh;">[@moleskyInverseDesignNanophotonics2018]</div>



### {data-background-image="fig/history_opt_phot.webp" data-background-opacity=1  data-background-size="90%" data-background-repeat="no-repeat"}


::: notes

During the second half of the twentieth century, advancement in fabrication capabilities allowed photonic engineering to expand into the microscale and nanoscale. Over the past two decades, this capability has led to the growth of a rich standard library of photonic design templates. From left to right, the examples shown for photonic miniaturization depict a Fabry–Pérot cavity, a fibre cavity and a microdisk resonator. a–f, The examples for the photonic design library are a PhC defect cavity from Painter et al.126 (a), a PhC fibre from a review by Knight127 (b), a micropost cavity from Pelton et al.87 (c), a microring resonator from Xu et al.128 (d), a nanobeam resonator from Eichenfield et al.129 (e) and a plasmonic sensor from Liu et al.130, with Ex denoting the electric field, Hy the magnetic field and kz the wave vector in Cartesian coordinates. (f). Bottom: a visual companion to the timeline of developments in photonic optimization described in the text. g–y, The images are: 1998–1999, a SiO2/SiON telecom-fibre to ridge-waveguide coupler from Spühler et al.26 (g) and an optimized two dimensional PhC from the work of Dobson and Cox27 (h); 2003–2008, a topology optimized ‘Z’ waveguide bend in a silicon-based PhC from Borel et al.41 (i), a single mode defect cavity from Frei et al.67 (j), a 1.50 × 1.55 μm demultiplexer from Håkansson and Sánchez-Dehesa48 (k), a level-set bandgap optimized PhC from Kao et al.44 (l), a 90° PhC waveguide bend form Jensen and Sigmund42 (m) and a nanoimprinted two-port demultiplexer from Borel et al.59 (n); 2008–2015 an optimized 90° waveguide bend from Tsuji and Hirayama61 (o), a cross-section of an inverse designed holey waveguide fibre from Lu et al.68 (p), a unit cell of a radial junction silicon wire array for solar absorption from Alaeian et al.9 (q), the solar absorption enhancement resulting from a computationally optimized surface texturing from Ganapati et al.10 (r), a three-dimensional fcc bandgap optimized PhC from Men et al.75 (s) and a transformation optics motivated waveguide bend from Liu et al.16 (t); 2015–2017, an optimized layered thermal emitter from Ilic et al.14 (u), a three-port silicon-on-insulator demultiplexer from Frellsen et al. 17 (v), a silicon-on-insulator two-port demultiplexer and broadband 1:3 power splitter from Piggott et al.110,113 (w), a topology optimized hyperlens and corresponding magnetic field magnitude profile from Otomori et al.131 (x) and a compact on-chip Fabry–Pérot resonator form Yu et al.132 (

:::


<!-- 
https://www.researchgate.net/profile/Roberto-Naboni/publication/323993071/figure/fig4/AS:607792511602688@1521920435451/Qatar-National-Convention-Centre-QNCC-2008_W640.jpg -->


