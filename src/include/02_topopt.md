
## Topology optimization: recipes


Material distribution in design domain $\Omega_{\rm des}$: density
function $p \in [0,1]$

### Filtering
- convolution: $f(\B r) = \frac{1}{A}{\rm exp}(-|\B r|^2 /R_f^2)$, with $\int_{\Omega_{\rm des}} f(\B r) =1$
\begin{equation*}
\densf(\B r)  = p * f = \int_{\Omega_{\rm des}} p(\B r') f(\B r -\B r') {\rm d} \B r'
\label{eq:gaussian_filt}
\end{equation*}
- PDE: $-R_f^2 \B\nabla ^2 \densf + \densf = p {\quad\rm on \,}\Omega_{\rm des}, \grad\densf\cdotp\B n = 0 {\quad\rm on \,}\partial\Omega_{\rm des}$
[@lazarovFiltersTopologyOptimization2011]

<!-- ### Filtering
<video data-autoplay src="fig/filter.mp4" style="width: 100%;"></video> -->

### Filtering
<img src="fig/filter.gif" style="width: 100%;" ></img>

### Projection 
$$\densp(\densf) = \frac{\tanh\left[\beta\nu\right] + \tanh\left[\beta(\densf-\nu)\right] }{\tanh\left[\beta\nu\right]
  + \tanh\left[\beta(1-\nu)\right]}$$
  with $\nu=1/2$ and $\beta>0$ increased during the course of the optimization.
[@wangProjectionMethodsConvergence2010]

<!-- ### Projection
<video data-autoplay src="fig/proj.mp4" style="width: 100%;"></video> -->

### Projection
<img src="fig/proj.gif" style="width: 100%;" ></img>

### Interpolation
$\varepsilon(\densp)=(\varepsilon_{\rm max}-\varepsilon_{\rm min})\,\densp^m + \varepsilon_{\rm min}$
[@bendsoeMaterialInterpolationSchemes1999]



::: notes

Exponent m: here it is linear in the following
Can also interpolate the inverse/log etc. Might be better for high contrast

:::


## Algorithm

::: columns

:::: {.column width=50% }


![](fig/topopt_flow.png){width=80%}

::::

:::: {.column width=50% }


- gradient based optimization algorithm: method of moving asymptotes (MMA[@svanbergClassGloballyConvergent]), free implementation via the `nlopt` package [@johnsonNLoptNonlinearoptimizationPackage]
- 40 iterations or until convergence on the objective or
design variables
-  repeated setting $\beta =2^n$, where $n$ is an integer between 0 and 7,
restarting the algorithm with the optimized density obtained at the
previous step

::::

:::