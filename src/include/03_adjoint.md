## Computing gradients

Solution vector $\B u$ we want to compute is parametrized by a vector of parameters $\B p$ of size $M$ and defined implicitly through an operator $\B F$ as:
\begin{equation}
\B F(\B u, \B p) = \B 0
\label{eq:genimpeq}
\end{equation}

$\B G$ is a functional of interest of dimension $N$, representing the quantity to be optimized

::: notes

for instance, the quality of a design to be maximized, or the error between a target and computations to be minimized. 
To obtain the derivative of the functional with respect to the design variables, several approaches can be used.

:::


### Finite differences
\begin{equation}
\frac{\mathrm{d}\B G}{\mathrm{d}p_i} \approx \frac{\B G(\B p + h \B e_i) - \B G(\B p)}{h}
\end{equation}
where $\B e_i$ is the vector with $0$ in all entries except for $1$ in the $i^{th}$ entry.


- numerical inaccuracy
- expensive for large $M$ and/or $N$

::: notes

However, in addition to the uncertainty on the choice of the parameter $h$ giving potentially numerical inaccuracy, this approach is prohibitively computationally demanding for large $M$ and/or $N$.

:::

### Tangent linear equation

Explicitly, the gradient can be computed applying the chain rule:
\begin{equation}
\frac{\mathrm{d}\B G}{\mathrm{d}\B p} =
\frac{\partial \B G}{\partial \B p} +
\frac{\partial \B G}{\partial \B u}
\frac{\mathrm{d}\B u}{\mathrm{d}\B p}.
\label{eq:gradG}
\end{equation}
Taking the total derivative of Eq. (\ref{eq:genimpeq}) we obtain the tangent linear equation:
\begin{equation}
{\frac{\partial \B F(\B u, \B p)}{\partial \B u}}
{\frac{\mathrm{d}\B u}{\mathrm{d}\B p}} =
{-\frac{\partial \B F(\B u, \B p)}{\partial \B p}}.
\end{equation} 

::: notes

This is a similar to forward mode differentiation and scales linearly with the number of inputs $M$. However, in typical topology optimization problems, the number of input parameters is generally much larger than the number of output objectives, so this technique is rather inefficient.

:::


### Adjoint equation

Assuming the tangent linear system is invertible, we can rewrite the Jacobian as:
\begin{equation}
\frac{\mathrm{d}\B u}{\mathrm{d}\B p} = - \left(\frac{\partial \B F(\B u, \B p)}{\partial \B u}\right)^{-1}
\frac{\partial \B F(\B u, \B p)}{\partial \B p}.
\end{equation}
After substituting this value in (\ref{eq:gradG}) and taking the adjoint (Hermitian transpose, denoted by $\dagger$) we get:
\begin{equation}
	\frac{\mathrm{d}\B G}{\mathrm{d}\B p}^{\dagger} =
	\frac{\partial \B G}{\partial \B p}^{\dagger}
	- \frac{\partial \B F(\B u, \B p)}{\partial \B p}^{\dagger} \left(\frac{\partial \B F(\B u, \B p)}{\partial \B u}\right)^{-\dagger}
	\frac{\partial \B G}{\partial \B u}^{\dagger} .
\end{equation}

### Adjoint equation
Defining the adjoint variable $\B \lambda$ as:
\begin{equation}
\B \lambda = \left(\frac{\partial \B F(\B u, \B p)}{\partial \B u}\right)^{-\dagger}
\frac{\partial \B G}{\partial \B u}^{\dagger}
\end{equation}
we obtain the adjoint equation
\begin{equation}
	\left(\frac{\partial \B F(\B u, \B p)}{\partial \B u}\right)^{\dagger} \B \lambda = \frac{\partial \B G}{\partial \B u}^{\dagger}.
\end{equation}

::: notes

For a given functional (output), the adjoint solution can be used to easily compute the gradient with respect to any parameter. Therefore, solving the adjoint system is extremely efficient when $M\gg N$. This approach is closely linked to reverse mode
differentiation in AD or backpropagation in the context of neural networks, since the flow of information in the equation system is reversed.
 
:::
