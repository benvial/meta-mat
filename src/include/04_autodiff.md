
## Automatic differentiation (AD)

- A general way of taking
a program which computes a value, and automatically constructing a
procedure for computing derivatives of that value, accurately to working precision, and using at most a small constant factor more arithmetic operations than the original program[@griewankEvaluatingDerivatives2008]
- Not finite differences / symbolic differentiation
- Procedure:
  1. Decompose original code into intrinsic functions (build computational graph)
  2. Differentiate the intrinsic functions, effectively symbolically
  3. Multiply together according to the chain rule
   
- Automation:
  - Source code transformation
  - Operator overloading


### Automatic differentiation (AD)

$f: \mathbb{R}^n \rightarrow \mathbb{R}^m$

Example: $f(x_1,x_2) = x_1x_2 + \sin(x_1)$

::: columns

:::: {.column width=50% }


Forward mode

![](fig/forward.png)


- more efficient if $m\gg n$

::::

:::: {.column width=50% }

Reverse mode

![](fig/reverse.png)

- more efficient if $n\gg m$
- need to store intermediate values

::::

:::
