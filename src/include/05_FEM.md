

## Maxwell's equations
Time-harmonic regime with a convention in $\exp(-i \omega t)$:

\begin{equation}
  \curl {\B H} =-i \omega \varepsilon_{0} \tens{\e} {\B E} 	\label{eq:maxwell1}
\end{equation}
\begin{equation}
  \curl {\B E} =i \omega \mu_{0} \tens{\mu}{\B H} \label{eq:maxwell2}
\end{equation}



## Finite Element Method (FEM)

- Scattering by an infinitely long object $\Omega_s$ embedded in a background with permittivity $\tens{\e_b}$ and permeability $\tens{\mu_b}$.
- Independent of $z$ and z-anisotropic materials:
\begin{equation}
	\tens{\e}=\ezaniso, \qquad \tens{\mu}=\muzaniso.
	\label{eq:zaniso}
\end{equation}

### Finite Element Method (FEM)

- The two polarizations decouple and Maxwell's equations can be combined into the following scalar propagation equation:
\begin{equation}
	\mathcal {M}_{\tens{\xi},\chi}(u) = \ddiv \left[\tens{\xi}\grad u\right]  + k_0^2 \chi u= 0,\label{eq:propa}
\end{equation}
  - TE polarization: $u=H_z$, $\tens{\xi} = \tens{\e_\parallel}^T/\det\tens{\e_\parallel}$, $\chi = \muzz$
  - TM polarization: $u=E_z$, $\tens{\xi} = \tens{\mu_\parallel}^T/\det\tens{\mu_\parallel}$, $\chi = \ezz$


### Finite Element Method

- Excitation $u_0$, the diffracted field $u_s = u - u_0$ must satisfy an Outgoing Waves Condition.
  - Plane wave: $u_0=\exp(i\B k\cdot\B r)$
  - Line source: Green's function $u_0=G(\B r,\B r',k)=-\frac{i}{4}H_0^{(1)}(k|\B r- \B r'|)$
- Scattering problem (\ref{eq:propa}) is recast into a radiation problem with a source $\mathcal {S}$ localized inside the object:
\begin{equation}
	\mathcal {M}_{\tens{\xi},\chi}(u_s) = -\mathcal {M}_{\tens{\xi}-\tens{\xi_b},\chi-\chi_b}(u_0) = \mathcal {S}.\label{eq:rad}
\end{equation}
- Weak form:
\begin{equation}
	\mathcal {R}_{\tens{\xi},\chi}(u,v) = -\int_\Omega  \,\tens{\xi}\grad u \grad v^{\star} {\dd\B r}+ \int_{\Omega} \, \chi u v^\star{\dd\B r} + \int_{\D\Omega}\,  (\tens{\xi}\grad u) v^\star {\dd s}.
	\label{eq:residual}
\end{equation}
Surface term = 0 since $(\tens{\xi}\grad u)\cdotp \B n = 0$ on the outer boundaries of the PMLs.
- Find $u\in\LSO$ such that:
\begin{equation}
	\mathcal {R}_{\tens{\xi},\chi}(u,v) = 0, \quad \forall v \in \LSO
	\label{eq:weak}
\end{equation}

<!-- -------------------------------------------------------------------------------- -->
### Application: cloaking


![](fig/cloak/figure1.png){.img width=60%}

[@vialTopologyOptimizedAlldielectric2015]

### Cloaking



> Objective: minimize scattered field in the background
> \begin{equation*}
>           \min_{p(\B r)} \quad \phi=\gamma\phi_1 + (1-\gamma) \phi_2 
>   \end{equation*}
> \begin{equation*} \phi_1=\frac{1}{W}\int_{\Omega_{\rm B}}\left|E_{\rm d}^{\rm cloak}\right|^2 {\rm d} \B r\end{equation*}
> \begin{equation*} \phi_2=\int_{\Omega_{\rm D}}\frac{h_{\rm m}^2}{S}\left| \B \nabla\,\varepsilon \right|^2 {\rm d} \B r\end{equation*}

- $W=\int_{\Omega_{\rm B}}\left|E_{\rm d}^{\rm object}\right|^2{\rm d} \B r$ (uncloaked case)
- $\phi_2$: alternative approach for filtering, $S=(R_{\rm c}-R_{\rm s})^2/2$ is the area of the design domain and $h_{\rm m}$ is the maximum size of a mesh element
- weight parameter $\gamma$ between 0 and 1

### Cloaking
![](fig/cloak/figure2.png){.img width=60%}


### Sensitivity to source

::: columns

:::: {.column width=50%}

 Correlation coefficient:
 \begin{equation*}
  \rho(X,Y)=\frac{ {\rm E}(XY) - {\rm E}(X){\rm E}(Y) }{ \sqrt{{\rm E}(X^2)-{\rm E}(X)^2} \sqrt{{\rm E}(Y^2)-{\rm E}(Y)^2} }
 \end{equation*}
![](fig/cloak/figure2bis.png){.img width=100%}
::::

:::: {.column width=50%}

![](fig/cloak/figure3.png){.img width=100%}

::::

:::

### Bandwidth enhancement with multi-frequency optimization

![](fig/cloak/figure4.png){.img width=50%}

### Modal analysis and the origin of cloaking effect
![](fig/cloak/figure5.png){.img width=50%}


<!-- -------------------------------------------------------------------------------- -->




### Illusion
> Objective: make a metallic cylinder look like an arbitrarily shaped dielectric
> \begin{equation*}
>           \min_{p(\B r)} \quad \phi=\gamma\phi_1 + (1-\gamma) \phi_2 
>   \end{equation*}
> \begin{equation*} \phi_1=\frac{1}{W}\int_{\Omega_{\rm B}}\left|E^{\rm ill} - E^{\rm ref}\right|^2 {\rm d} \B r\end{equation*}
> \begin{equation*} \phi_2=\int_{\Omega_{\rm D}}\frac{h_{\rm m}^2}{S}\left| \B \nabla\,\varepsilon \right|^2 {\rm d} \B r\end{equation*}

- $W=\int_{\Omega_{\rm B}}\left|E^{\rm ref}\right|^2{\rm d} \B r$ (reference object only)


### Illusion
![](fig/illusion/figure1.png){.img width=50%}
[@vialOptimizedMicrowaveIllusion2017]

### Experimental setup
![](fig/illusion/twoDmapper2.png){.img width=50%}

### Experimental results
![](fig/illusion/figure2.png){.img width=100%}

### Experimental results
![](fig/illusion/figure3.png){.img width=80%}



<!-- -------------------------------------------------------------------------------- -->
### FEM: implementation
Open source libraries with bindings for the `python` programming language using a custom code `gyptis`.

- Geometry and mesh generation: `gmsh`
- FEM library: `fenics` using second order Lagrange basis functions
- Gradient calculations: `dolfin-adjoint` library with automatic differentiation 

[@gyptis042]
[@geuzaineGmsh3DFinite2009]
[@alnaesFEniCSProjectVersion2015] 
[@mituschDolfinadjoint2018Automated2019] 


<!-- -------------------------------------------------------------------------------- -->


### Bi-focal lens
> Objective: focal point at two different locations depending on the excitation frequency
> \begin{equation*}
>           \max_{p(\B r)} \quad \Phi = \left|E_1(\omega_1,\B r_1)\right| + \left|E_2(\omega_2,\B r_2)\right|
>   \end{equation*}


### Optimization history
![](fig/bifoc/figure1.png){.img width=90%}

### Bi-focal lens
![](fig/bifoc/lens.png){.img width=50%}
[@vialOptimizationExperimentalValidation2022]

### Measurements
![](fig/bifoc/figure2.png){.img width=70%}

### Measurements
![](fig/bifoc/figure3.png){.img width=90%}


<!-- -------------------------------------------------------------------------------- -->


### Ferrorelectric metamaterials
![](fig/ferro/unit_cell.png){.img width=50%}
[@vialEnhancedTunabilityFerroelectric2019]

### Nonlinear permittivity

::: columns
:::: {.column width=50%}
![](fig/ferro/epsilon_fit.png){.img width=100%}
::::
:::: {.column width=50%}
\begin{equation*} \ef(E) = \frac{ \ef(0)}{ \left[(\xi +1)^{1/2} +\xi \right]^{2/3} +\left[(\xi +1)^{1/2} - \xi \right]^{2/3} -1} \label{eq:nl_eps}\end{equation*}
with $\xi=E/E_{\rm N}$, $\ef(0)=3050$ and $E_{\rm N}=1.65\,$kV/mm at DC,
$\ef(0)=165$ and $E_{\rm N}=1.12\,$kV/mm at $f=3.8\,$GHz.
::::
:::

[@vendikMicrowaveLossesIncipient1998]


### Nonlinear permittivity coupled with electrostatics
Solve for the potential $V$ satisfying \rl{Gauss's} law for a given
permittivity distribution $\e$:
\begin{equation} \ddiv (\e(\B E) \grad V) = 0 \label{eq:elstat}\end{equation}
biased by a constant uniform electric field $\B E_{\rm B} =\Eb \B x$.
The electric field $\B E=-\grad V$ derived from the solution depends on the permittivity distribution, which itself
depends on the electric field.

### Homogenized properties

Two scale expansion
$\Hm(\B r)=\Hm_0(\B r,\B r/\nu) + \nu \Hm_1(\B r,\B r/\nu) + \nu^2 \Hm_2(\B r,\B r/\nu) + ...$
for magnetic field $\Hm$

Two annex problems $\mathcal P_j$, $j={x, y}$ on the unit cell :
\begin{equation} \ddiv \left[ \e(\B E) \grad(\psi_j + r_j) \right] = 0, \label{eq:hom_annex}\end{equation}
The homogenized permittivity $\ehom$ is then obtained as:
\begin{equation} \ehom(\B E) = \langle \e(\B E)\rangle \B I + \B \phi(\B E), \label{eq:hom_epsi}\end{equation}
where $\mn{.}$ denotes
the mean value over the unit cell, $\B I$ is the $2\times 2$ identity
matrix and $\B \phi_{ij}(\B E) = \langle \e(\B E) \grad \psi_i(\B E) \rangle_j$ are
correction terms.

[@allaireHomogenizationTwoScaleConvergence1992]


### Enhancement of tunability
![](fig/ferro/convergence_per.png){.img width=100%}

### Optimized ferroelectric metamaterials
> Objective: maximize the tunability of the composites
> along the direction of the applied electric field defined as
> $\tunh(E)=\exxh(0)/\exxh(E)$
> \begin{equation} 
>     \begin{aligned} 
>       \max_{p(\B r)} \quad & \sigma(E) = \frac{\tunh(E)}{\eta(E)}-1\\ 
>       \textrm{s.t.} \quad & f_{\rm min} < f < f_{\rm max}\\ 
>     \end{aligned} 
>   \label{eq:max_tuna} 
> \end{equation} 
where $\sigma$ is the tunability enhancement compared to
the bulk BST tunability $\eta(E)=\ef(0)/\ef(E)$. 

[@vialHighFrequencyMetaferroelectrics2021]

### Optimized ferroelectric metamaterials

::: columns
:::: {.column width=50%}
![](fig/ferro/anim_conv_80.0.gif){.img width=100%}
::::
:::: {.column width=50%}
![](fig/ferro/anim_dens_80.0.gif){.img width=100%}
::::
:::

### Optimized ferroelectric metamaterials
![](fig/ferro/fig1.png){.img width=90%}



### Bi-objective optimization

Loss reduction factor: 
\begin{equation} \theta(E) = 1- \frac{\tdhxx(E)}{\tdf(E)} \label{eq:loss_red_factor} \end{equation}
where the homogenized loss tangent $xx$ component is
$\tdhxx(E) = -\im \exxh(E) / \re\exxh(E)$.

> Objective: high tunability and low loss
> \begin{equation} \begin{aligned} \min_{p(\B r)} \quad & \gamma\,\sigma(E) + (1-\gamma)\,\theta(E)\\ \textrm{s.t.} \quad & f_{\rm min} < f < f_{\rm max}\ \end{aligned} \label{eq:biobj} \end{equation}

### Bi-objective optimization

::: columns
:::: {.column width=50%}
![](fig/ferro/convergence_alpha_50.gif){.img width=100%}
::::
:::: {.column width=50%}
![](fig/ferro/density_alpha_50.gif){.img width=100%}
::::
:::

### Bi-objective optimization
![](fig/ferro/biobj.eps.png){.img width=90%}


<!-- -------------------------------------------------------------------------------- -->

### Inverse design of superscatterers

- Design domain: circular rod of diameter $D=2R=\lambda_0=600$nm, materials: $\rm SiO_2$ ($\varepsilon_{\rm min}=2.17$) and silicon ($\varepsilon_{\rm max}=15.59-0.22i$).
- Scattering width computed on a circle $\Gamma$ enclosing the object:
\begin{equation}
\sigma_s=\frac{1}{|\B S_i|}\int_\Gamma  \B n \cdotp \B S_s {\rm d} \Gamma
  \label{eq:scs}
\end{equation}
with Poynting vectors $\B S_i = \frac{1}{2} {\rm Re}[\B E_i \times \B H_i^\star]$ and $\B S_s = \frac{1}{2} {\rm Re}[\B E_s \times \B H_s^\star]$ associated with the incident and scattered field respectively, $\B n$ is the unit vector normal to $\Gamma$.

 > *Objective*: maximize the normalized scattering width:
    \begin{equation}
        \max_{p(\B r)} \quad    \Phi = \sigma_s/2R
    \end{equation}


<!-- ### {data-background-image="fig/animation_gyptis_TE.mp4"}  -->

### History

<!-- <div style="width: 60%;">
</div> -->

::: columns

:::: {.column width=50%}

TE
<video data-autoplay src="fig/animation_gyptis_TE.mp4"></video>

::::

:::: {.column width=50%}

TM
<video data-autoplay src="fig/animation_gyptis_TM.mp4"></video>

::::

:::

### Superscatterers: results

![](fig/figure1.png)

### Superscatterers: modal analysis

![](fig/figure2.png)