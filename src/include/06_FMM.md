
## Fourier Modal Method (FMM)


::: columns

:::: {.column width=50% }

- AKA Rigorous Coupled Wave Analysis (RCWA)
- Suited for a specific type of periodic structure made up of stacked structured layers
- *Key idea*: expand the electromagnetic
fields within each layer into eigenmodes represented using a Fourier basis in the plane of periodicity

[@whittakerScatteringmatrixTreatmentPatterned1999]

[@liuS4FreeElectromagnetic2012]

::::

:::: {.column width=50% }

![](fig/metasurface_nannos.png)
 

::::

:::



### FMM

- Rescaled magnetic field $\tilde{\B H} = \sqrt{\mu_0/\varepsilon_0}\B H$ expanded as:
\begin{equation}
\tilde{\B H}(\rpara, z)=\sum_{\B {G}} \B{H}_{\B {G}}(z) {\rm e}^{i(\B {k}+\B {G}) \cdot \rpara},
\label{eq:Hexpansion}
\end{equation}
with $\B {G} = n L_{k,1} + m L_{k,2}$ a reciprocal lattice vector and $\rpara$ the in plane component of the position vector
- Fourier transform:
$$
\varepsilon_{\B{G}}=\frac{1}{\left|L_{r}\right|} \int_{\Omega} \varepsilon(\B{r}) e^{-i \B{G} \cdot \B{r}} d \B{r}
$$
where $\Omega$ denotes one unit cell of the lattice
- Form block Toepliz matrices $\hat{\varepsilon}_{m n}=\varepsilon_{\left(\B{G}_{m}-\B{G}_{n}\right)}$ for each components of $\B \varepsilon$.


### FMM

Fourier coefficients $d_{x}$ and $d_{y}$ of the displacement field $\B{D}$ related to the electric field by:
\begin{equation}
\left[\begin{array}{c}-d_{y}(z) \\ d_{x}(z)\end{array}\right]=\mathcal{E}\left[\begin{array}{c}-e_{y}(z) \\ e_{x}(z)\end{array}\right]
\label{eq:DE}
\end{equation}

- Early formulation: [@moharamRigorousCoupledwaveAnalysis1981] Laurent’s rule
$$\mathcal{E}=\left[\begin{array}{ll}\hat{\epsilon}_{x x} & \hat{\epsilon}_{x y} \\ \hat{\epsilon}_{y x} & \hat{\epsilon}_{y y}\end{array}\right].$$
- Improved convergence: proper factorization Rules (Li, 1997 [@liNewFormulationFourier1997]): decompose the electric field in
its normal and tangential components at material interfaces (normal vector field generation)[@schusterNormalVectorMethod2007] 

### FMM
- Anzatz for eigenmode: 
\begin{equation*}\B{H}(z)=\sum_{\B{G}}\left[\phi_{\B{G}, x} \B{x}+\phi_{\B{G}, y} \B{y}-\frac{\left(k_{x}+G_{x}\right) \phi_{\B{G}, x}+\left(k_{y}+G_{y}\right) \phi_{\B{G}, y}}{q} \B{z}\right] e^{i(\B{k}+\B{G}) \cdot \B{r}+i q z}\end{equation*}
- Fourier transform + eliminating $z$ components, we get the EVP:
\begin{equation*}
M \Phi = \left(\mathcal{E}\left(k_0^{2}-\mathcal{K}\right)-K\right) \Phi= Q^{2}\Phi, \quad \Phi=\left[\begin{array}{l}
\phi_{x} \\
\phi_{y}
\end{array}\right]
\end{equation*}
$Q^{2}={\rm diag\,} q_n^2$, eigenvalues $q_{n}^2$, $\left[\phi_{x, n}, \phi_{y, n}\right]^{\rm T}$ Fourier coefficients of the eigenmodes
\begin{equation*}
	\mathcal{K}=\left[\begin{array}{cc}\hat{k}_{y} \hat{\varepsilon}_{z}^{-1} \hat{k}_{y} & -\hat{k}_{y} \hat{\varepsilon}_{z}^{-1} \hat{k}_{x} \\ -\hat{k}_{x} \hat{\varepsilon}_{z}^{-1} \hat{k}_{y} & \hat{k}_{x} \hat{\varepsilon}_{z}^{-1} \hat{k}_{x}\end{array}\right], \quad
	K=\left[\begin{array}{ll}\hat{k}_{x} \hat{k}_{x} & \hat{k}_{x} \hat{k}_{y} \\ \hat{k}_{y} \hat{k}_{x} & \hat{k}_{y} \hat{k}_{y}\end{array}\right],
\end{equation*}
where $\hat{k}_{\nu}$, $\nu\in\{x,y\}$, are diagonal matrices with entries $\left(k_{\nu}+G_{1 \nu}, k_{\nu}+G_{2 \nu}, \ldots\right)$.


### FMM

- Compute fields in each layer
- $S$-matrix algorithm is then used starting from the incident medium to
recursively find the $S$-matrix of each layer and form the total $S$-matrix.
- Computation of relevant electromagnetic quantities of interest, e.g.  diffraction efficiencies in transmission and reflection for each order by calculating the power flux through the outermost layer (more efficient in Fourier space)
[@whittakerScatteringmatrixTreatmentPatterned1999]

### Implementation

FMM and PWEM in `python` with various numerical backends for core linear algebra operations and array manipulation

- `numpy` [@harris2020array] 
- `scipy` [@2020SciPy-NMeth]
- `autograd` (AD)[@maclaurin2015autograd]
- `pytorch` (AD + GPU)[@NEURIPS2019_9015;@paszke2017automatic]
- `jax` (AD + GPU) [@jax2018github] 

::: notes

jax: AD not implemented for non-Hermitian eigenproblems for gradient w.r.t. eigenvectors

:::

### FMM benchmark

::: columns

:::: {.column width=50% }

![](fig/speedup1.png)

::::

:::: {.column width=50% }

![](fig/speedup16.png)

::::

:::


### Deflective metasurface

- $\lambda_t=732$nm, periods $L_x=800$nm and $L_y=400$nm, 
plane wave normally incident from the superstrate (silica, $\varepsilon=2.16$).
- Design domain: metasurface layer of thickness $350$nm, restricted to one unit cell, $\varepsilon_{\rm min}=1$ (air) and $\varepsilon_{\rm max}=14.06-0.074i$ (silicon)

> *Objective*: maximize the average of the transmission coefficient in the $(1,0)$ diffracted order for both polarizations:
	\begin{equation}
		\max_{p(\B r)} \quad    \Phi = \frac{1}{2} \left( T^{\rm TE}_{(1,0)} + T^{\rm TM}_{(1,0)}\right)
	\end{equation}

###
<video data-autoplay src="fig/animation_nannos.mp4" style="width: 150%;"></video>

###
![](fig/figure3.png){.img width=100%}


###
![](fig/figure4.png){.img width=100%}


### {data-background-color=black data-background-image="fig/metasurface.png"}

