
## Plane Wave Expansion Method



- 2D, possibly $z$-anisotropic materials in $\varepsilon$ and $\mu$, non dispersive
- Polarization decouple, expand the $z$ components as:
\begin{equation}
u(\B{r})=\sum_{\B {G}} u_{\B {G}}\, {\rm e}^{i(\B {k}+\B {G}) \cdot \B{r}},
\label{eq:pwem1}
\end{equation}
- After Fourier transforming Maxwell's equations and recombining
the relevant $z$ component of the fields, we get the following generalized eigenproblem:
\begin{equation}
\mathcal{Q}^{\rm T} \,\hat{\tens{\theta}_\parallel}^{-1}\,\mathcal{Q}\, \Phi = k_0^2 \,\chi_{zz}\, \Phi
\label{eq:pwem2}
\end{equation}
$\tens{\theta}_\parallel=\tens{\mu}_\parallel$ for TM and $\tens{\varepsilon}_\parallel$ for TE polarization, $\mathcal{Q} =  \left[\hat{k}_{y}, -\hat{k}_{x}\right]^{\rm T}$ and
$\Phi=\left[u_{\B{G}_{1}}, u_{\B{G}_{2}}, \ldots\right]^{\rm T}$
- Reduced Bloch Mode Expansion [@husseinReducedBlochMode2009], only solving Eq. (\ref{eq:pwem2}) at symmetry points of the first Brillouin zone and performing a second expansion using those modes as a basis set.



### Photonic crystals: maximizing bandgaps


- TE modes, square array with enforced $C_4$ symmetry on the unit cell, $\varepsilon_{\rm min}=1$ (air) and $\varepsilon_{\rm max}=9$

> Objective: open and maximize a bandgap between the $5^{th}$ and $6^{th}$ eigenvalues:\begin{equation*}\max_{p(\B r)} \quad    \Phi = \min_{\B k} \omega_{6}(\B k) - \max_{\B k} \omega_{5}(\B k)\end{equation*}

- Final distribution in agreement with simple geometrical rules: the walls of an optimal centroidal Voronoi tessellation with $n=5$ points[@sigmundGeometricPropertiesOptimal2008]


###

<video data-autoplay src="fig/animation_protis_bandgap_TE.mp4" style="width: 66%;"></video>


###
![](fig/figure5a.png){.img width=66%}


### Photonic crystals: dispersion engineering

- TM modes, symmetry with respect to $y$

> *Objective*: obtain a prescribed dispersion curve for the $6^{th}$ band \begin{equation*}\min_{p(\B r)} \quad \Phi =  \left\langle\left|\omega_{6}(k_x) - \langle \omega_{6}\rangle - \omega_{\rm tar}(k_x) \right|^2\right\rangle \end{equation*}
> with \begin{align*}\omega_{\rm tar}(k_x) =& -0.02 \cos(k_x  a) + 0.01 \cos(2  k_x a) \\ &+ 0.007 \cos(3  k_x  a)\end{align*}
> $\langle f\rangle =\frac{1}{M}\sum_{m=0}^M f_m$


###

<video data-autoplay src="fig/animation_protis_dispersion_TM.mp4" style="width: 66%;"></video>

###
![](fig/figure5b.png){.img width=66%}

