


## Open source

- Free software: low cost, portable, customizable, vendor-independent
- Widely used programming language, is easily installable and integrates with the rich and growing scientific Python ecosystem
- Reproducible and collaborative research
- Auto-differentiation: inverse design of photonic structures and metamaterials with improved performances and  explore intriguing effects



### Get the code

- Development on gitlab: continuous integration for testing and documentation deployment
- Install / fork it / run it online / report bugs!

::: columns

:::: {.column width=50% }

FEM
```bash
pip install gyptis
```
```bash
conda install -c conda-forge gyptis
```
FMM
```bash
pip install nannos
```
```bash
conda install -c conda-forge nannos
```
PWEM
```bash
pip install protis
```

::::

:::: {.column width=50% }

<p style="margin-top:2em;">
![[gyptis.gitlab.io](https://gyptis.gitlab.io)](fig/gyptis-name.png){.img width=33%}
</p>
<p style="margin-top:3em;">
![[nannos.gitlab.io](https://nannos.gitlab.io)](fig/nannos-name.png){.img width=33%}
</p>
<p style="margin-top:3em;">
![[protis.gitlab.io](https://protis.gitlab.io)](fig/protis-name.png){.img width=33%}
</p>


::::

:::


<section data-background-iframe="https://gyptis.gitlab.io"
          data-background-interactive>
</section>



### {data-background-color=black data-background-image="fig/gyptis_binder.png" data-background-opacity=0.}

### {data-background-image="fig/gyptis_binder.png"}

<div style="position: absolute; width: 25%; right: 0; box-shadow: 0 1px 4px rgba(0,0,0,0.5), 0 5px 25px rgba(0,0,0,0.2); background-color: rgba(0, 0, 0, 0.9); color: #fff; padding: 20px; font-size: 166%; text-align: left;">
    Online example
</div>

## {data-background-color="var(--Navy)"}
<h2 class="r-fit-text">THANK YOU</h2>


![](tex/themes/imperial-mono.png){.img width=33%}